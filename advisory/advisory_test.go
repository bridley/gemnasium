package advisory

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v2"
)

func TestAdvisory_Decode(t *testing.T) {
	tcs := []struct {
		name string
		path string
		want Advisory
	}{
		{
			"Minimal",
			"testdata/gem/actionmovie/GMS-2019-minimal.yml",
			Advisory{
				Identifier:     "GMS-2019-minimal",
				Title:          "Unsafe UPDATE query",
				Description:    "There is a vulnerability in some UPDATE query.",
				DisclosureDate: "2019-10-30",
				AffectedRange:  "*",
				Package: Package{
					Type: "gem",
					Name: "actionmovie",
				},
			},
		},
		{

			"Medium",
			"testdata/gem/activerecord/CVE-2016-6317.yml",
			Advisory{
				Identifier:       "CVE-2016-6317",
				Title:            "Unsafe Query Generation Risk",
				Description:      "There is a vulnerability when Active Record is used in conjunction with JSON\r\nparameter parsing. This vulnerability is similar to CVE-2012-2660,\r\nCVE-2012-2694 and CVE-2013-0155.",
				DisclosureDate:   "2016-08-11",
				AffectedRange:    ">=4.0.0.alpha <4.2.7.1",
				FixedVersions:    []string{"4.2.7.1"},
				ImpactedVersions: "4.x",
				NotImpacted:      "5.x, 3.x and earlier",
				Solution:         "Upgrade to latest or use workaround; see provided link.",
				Credit:           "joernchen of Phenoelit",
				Links: []string{
					"https://groups.google.com/forum/#!topic/rubyonrails-security/rgO20zYW33s",
				},
				Package: Package{
					Type: "gem",
					Name: "activerecord",
				},
				UUID: "51b1a6d4-3eb3-48d4-8d25-ae62c2dbb3d4",
			},
		},
		{

			"Full",
			"testdata/go/github.com/facebook/fbthrift/thrift/lib/go/thrift/CVE-2019-3564.yml",
			Advisory{
				Identifier:       "CVE-2019-3564",
				Identifiers:      []string{"CVE-2019-3564", "GHSA-123-123", "GMS-2020-1"},
				Title:            "Improper Input Validation",
				Description:      "Description.",
				DisclosureDate:   "2019-10-09",
				AffectedRange:    "<v2019.03.04.00",
				FixedVersions:    []string{"v2019.03.04.00"},
				NotImpacted:      "All versions starting from 2019.03.04.00",
				ImpactedVersions: "All versions before 2019.03.04.00",
				Solution:         "Upgrade to version 2019.03.04.00 or above.",
				Links: []string{
					"https://nvd.nist.gov/vuln/detail/CVE-2019-3564",
					"https://www.facebook.com/security/advisories/cve-2019-3564",
				},
				Package: Package{
					Type: "go",
					Name: "github.com/facebook/fbthrift/thrift/lib/go/thrift",
				},
				UUID: "2582019a-7398-42f7-8d75-bfcee0dd1e87",
				Versions: []Version{
					{
						Number: "v2019.03.04.00",
						Commit: Commit{
							Tags: []string{
								"v2019.03.04.00",
							},
							Sha:       "fc82aa6cfdb437dabf18c606ed23129e5973b5ea",
							Timestamp: "20190304035542",
						},
					},
				},
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			f, err := os.Open(tc.path)
			require.NoError(t, err)
			defer f.Close()

			got := Advisory{}
			err = yaml.NewDecoder(f).Decode(&got)
			require.NoError(t, err)
			require.Equal(t, tc.want, got)
		})
	}
}
