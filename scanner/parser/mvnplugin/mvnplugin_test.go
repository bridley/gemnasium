package mvnplugin

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestMvnplugin(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tcs := []struct {
			name    string // test case name
			dir     string // dir where fixtures are located
			fixture string // fixture file name
			opts    parser.Options
		}{
			{
				name:    "maven simple",
				dir:     "simple",
				fixture: "maven-packages.json",
				opts:    parser.Options{},
			},
			{
				name:    "maven big",
				dir:     "big",
				fixture: "maven-packages.json",
				opts:    parser.Options{},
			},
			{
				name:    "gradle",
				dir:     "gradle",
				fixture: "gradle-dependencies.json",
				opts:    parser.Options{},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.dir, tc.fixture)
				got, _, err := Parse(fixture, tc.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc.dir, got)
			})
		}
	})
}
