package convert

import (
	"sort"

	"gonum.org/v1/gonum/graph/path"
	"gonum.org/v1/gonum/graph/simple"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Node combines a package with its IID.
type Node struct {
	IID     uint
	Package *parser.Package
}

// ID returns an identifier that is unique in the scope of a graph.
// It is needed so that the Node implements the graph.Node interface.
func (n Node) ID() int64 {
	return int64(n.IID)
}

// NewGraph generates a dependency graph for a list of packages and dependencies.
// It takes an index that is used to resolve packages to node IDs.
func NewGraph(pkgs []parser.Package, deps []parser.Dependency, index *Index) *Graph {
	// build directed graph
	dg := simple.NewDirectedGraph()

	// convert package to node
	toNode := func(pkg *parser.Package) *Node {
		if pkg == nil {
			return &Node{0, pkg}
		}
		id := index.IDOf(*pkg)
		return &Node{id, pkg}
	}

	// add nodes
	for _, pkg := range pkgs {
		p := pkg // prevent implicit memory aliasing in for loop, gosec G601
		node := toNode(&p)
		existingNode := dg.Node(node.ID())

		if existingNode != nil {
			// a node with the given package name and version already exists in the dependency graph, skip it
			continue
		}

		dg.AddNode(node)
	}

	// add edges
	for _, dep := range deps {
		from := toNode(dep.From)
		to := toNode(dep.To)

		// skip self-edge because it is not supported by simple.DirectedGraph,
		// and because it does not provide valuable information
		if from.ID() == to.ID() {
			continue
		}

		e := dg.NewEdge(from, to)
		dg.SetEdge(e)
	}

	// shortest paths
	shortest, _ := path.JohnsonAllPaths(dg)

	return &Graph{index, dg, &shortest}
}

// Graph is the dependency graph of a scanned project.
// It is used to calculate the dependency path to any project dependency.
type Graph struct {
	index    *Index
	graph    *simple.DirectedGraph
	shortest *path.AllShortest
}

// PathTo returns one of the shortest dependency paths to the given package.
// The path goes from a direct project dependency to a parent of the given package.
// It is empty if the given package is a direct dependency of the project.
func (g Graph) PathTo(pkg parser.Package) []Node {
	nodeID := int64(g.index.IDOf(pkg))
	paths, _ := g.shortest.AllBetween(0, nodeID)
	nodes := []Node{}
	if len(paths) == 0 {
		return nodes
	}

	// check for 2-node path: this is a direct dependency since paths[0][0] is a nil node,
	// paths[0][1] will be the pkg itself
	if len(paths[0]) < 2 {
		return nodes
	}

	// convert nodes of first path
	sort.Slice(paths, func(i, j int) bool {
		return paths[i][1].ID() < paths[j][1].ID()
	})
	for _, gnode := range paths[0] {
		switch gnode.ID() {
		case 0:
			// skip root node
		case nodeID:
			// skip target node
		default:
			// append to node list
			node, _ := gnode.(*Node)
			nodes = append(nodes, *node)
		}
	}
	return nodes
}
