package convert

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"

// NewIndex returns a map with package key as the key, and package iid as the value.
//
// Example:
//
//  {
//    "Antlr3.Runtime:3.5.1":38,
//    "System.Text.RegularExpressions:4.3.0":42,
//    "Remotion.Linq:2.2.0":51,
//  }
func NewIndex(pkgs []parser.Package) *Index {
	pkgMap := map[string]uint{}
	for i, pkg := range pkgs {
		pkgMap[pkgKey(pkg)] = uint(i + 1)
	}
	return &Index{pkgMap}
}

// Index is an index of packages
type Index struct {
	// pkgMap maps package keys to package IIDs
	pkgMap map[string]uint
}

// IDOf returns the ID of a package
func (idx Index) IDOf(pkg parser.Package) uint {
	id, _ := idx.pkgMap[pkgKey(pkg)]
	return id
}

// pkgKey returns the key of a package.
// The key combines the package name with the package version,
// and is unique in the scope of a dependency file.
func pkgKey(pkg parser.Package) string {
	return pkg.Name + ":" + pkg.Version
}
