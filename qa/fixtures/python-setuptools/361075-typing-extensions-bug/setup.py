from setuptools import setup

setup(
    name = "test project",
    version = "0.0.1",
    install_requires=[
        'keyring==23.3.0',
    ]
)
